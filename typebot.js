/*jslint browser:true*/
(function (doc, cont) {
    'use strict';

    var fn, el, textCache, text, typed, strings;

    fn = {};
    el = {};
    textCache = '';
    text = [];
    typed = [];
    strings = {
        inputClass: 'txtInput',
        raceAgainClass: 'raceAgainLink',
        promptClass: 'challengePromptDialog'
    };

    fn.prep = function (e) {
        textCache = document.getElementsByClassName('nonHideableWords')[0].innerText;
        text = textCache.split(' ');
    };

    fn.typeNext = function () {
        var word;
        word = text.shift();
        el.inpt.value = word;
        typed.push(word);
    };

    fn.reset = function () {
        textCache = "";
        text = [];
        typed = [];
    };

    fn.inputEmpty = function () {
        return el.inpt.value.trim() === "";
    };

    fn.nothingToType = function () {
        return text.length === 0;
    };

    fn.type = function (e) {
        el.inpt = e.target;

        // Return 
        if (!fn.inputEmpty()) {
            return;
        }

        e.preventDefault();

        if (fn.nothingToType()) {
            fn.prep();
        }

        fn.typeNext();
    };

    fn.run = function () {
        el.container = cont;

        el.container.addEventListener('keydown', function (e) {
            if (e.target.className.indexOf(strings.inputClass) !== -1) {
                fn.type(e);
            }
        });

        el.container.addEventListener('click', function (e) {
            if (e.target.className.indexOf(strings.raceAgainClass) !== -1) {
                fn.reset(e);
            }
        });
        
        document.getElementsByTagName('body')[0].addEventListener('keydown', function () {
            var els, el;
            els = document.getElementsByClassName(strings.promptClass);
            if (els.length > 0) {
                el = els[0];
                el.parentNode.removeChild(el);
            }
        });

        console.log('TypeBot loaded...');
    };

    window.typebot = fn;
    window.tbEl = el;
}(document, document.getElementsByClassName('mainViewportHolder')[0]));